﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using wServer.networking;
using wServer.networking.svrPackets;
using wServer.realm.entities;
using wServer.realm.entities.player;
using wServer.realm.setpieces;
using wServer.realm.worlds;

#endregion

namespace wServer.realm.commands
{
    internal class posCmd : Command
    {
        public posCmd()
            : base("position", 1)
        {
        }

        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            player.SendInfo("X: " + (int)player.X + " - Y: " + (int)player.Y);
            return true;
        }
    }

    internal class MaxPlayer : Command
    {
        public MaxPlayer()
            : base("maxplayer", 5)
        {
        }

        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            if (string.IsNullOrEmpty(args[0]))
            {
                player.SendHelp("Usage: /maxplayer <player>");
                return false;
            }
            var plr = player.Manager.FindPlayer(args[0]);
            try
            {
                plr.Stats[0] = plr.ObjectDesc.MaxHitPoints;
                plr.Stats[1] = plr.ObjectDesc.MaxMagicPoints;
                plr.Stats[2] = plr.ObjectDesc.MaxAttack;
                plr.Stats[3] = plr.ObjectDesc.MaxDefense;
                plr.Stats[4] = plr.ObjectDesc.MaxSpeed;
                plr.Stats[5] = plr.ObjectDesc.MaxHpRegen;
                plr.Stats[6] = plr.ObjectDesc.MaxMpRegen;
                plr.Stats[7] = plr.ObjectDesc.MaxDexterity;
                plr.SaveToCharacter();
                plr.Client.Save();
                plr.UpdateCount++;
                plr.SendInfo("You have been maxed by: " + player.Name);
            }
            catch
            {
                player.SendError("Error while maxing players stats!");
                return false;
            }
            return true;
        }
    }

    internal class LeftToMax : Command
    {
        public LeftToMax()
            : base("lefttomax", 0)
        {
        }
        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            int Hp = player.ObjectDesc.MaxHitPoints - player.Stats[0];
            int Mp = player.ObjectDesc.MaxMagicPoints - player.Stats[1];
            int Atk = player.ObjectDesc.MaxAttack - player.Stats[2];
            int Def = player.ObjectDesc.MaxDefense - player.Stats[3];
            int Spd = player.ObjectDesc.MaxSpeed - player.Stats[4];
            int Vit = player.ObjectDesc.MaxHpRegen - player.Stats[5];
            int Wis = player.ObjectDesc.MaxMpRegen - player.Stats[6];
            int Dex = player.ObjectDesc.MaxDexterity - player.Stats[7];
            player.SendInfo(Hp / 5 + " Till maxed Health");
            player.SendInfo(Mp / 5 + " Till maxed Mana");
            player.SendInfo(Atk + " Till maxed Attack");
            player.SendInfo(Def + " Till maxed Defense");
            player.SendInfo(Spd + " Till maxed Speed");
            player.SendInfo(Vit + " Till maxed Vitality");
            player.SendInfo(Wis + " Till maxed Wisdom");
            player.SendInfo(Dex + " Till maxed Dexterity");
            return true;
        }
    }

    internal class RogueCommand : Command
    {
        public RogueCommand() : base("rogue", 1)
        {
        }
        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            for (int i = 0; i < player.Inventory.Length; i++)
            {
                if (player.Inventory[4] != null) { return false; }
                if (player.Inventory[5] != null) { return false; }
                if (player.Inventory[6] != null) { return false; }
                if (player.Inventory[7] != null) { return false; }
                else
                {
                    player.Inventory[4] = player.Manager.GameData.Items[0xa19]; // Fire dagger
                    player.Inventory[5] = player.Manager.GameData.Items[0xa59]; // Red agent
                    player.Inventory[6] = player.Manager.GameData.Items[0xa0e]; // Studded
                    player.Inventory[7] = player.Manager.GameData.Items[0xabd]; // Superior
                    player.UpdateCount++;
                }
            }
            player.SendInfo("Set Given");
            return true;
        }
    }
    internal class ArcherCommand : Command
    {
        public ArcherCommand() : base("archer", 1)
        {
        }
        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            for (int i = 0; i < player.Inventory.Length; i++)
            {
                if (player.Inventory[4] != null) { return false; }
                if (player.Inventory[5] != null) { return false; }
                if (player.Inventory[6] != null) { return false; }
                if (player.Inventory[7] != null) { return false; }
                else
                {
                    player.Inventory[4] = player.Manager.GameData.Items[0xa1e]; // Golden bow
                    player.Inventory[5] = player.Manager.GameData.Items[0xa64]; // t4 quiver
                    player.Inventory[6] = player.Manager.GameData.Items[0xa0e]; // Studded
                    player.Inventory[7] = player.Manager.GameData.Items[0xabd]; // Superior
                    player.UpdateCount++;
                }
            }
            player.SendInfo("Set Given");
            return true;
        }
    }
    internal class WizardCommand : Command
    {
        public WizardCommand() : base("wizard", 1)
        {
        }
        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            for (int i = 0; i < player.Inventory.Length; i++)
            {
                if (player.Inventory[4] != null) { return false; }
                if (player.Inventory[5] != null) { return false; }
                if (player.Inventory[6] != null) { return false; }
                if (player.Inventory[7] != null) { return false; }
                else
                {
                    player.Inventory[4] = player.Manager.GameData.Items[0xa9f]; // horror
                    player.Inventory[5] = player.Manager.GameData.Items[0xad6]; // t4 spell
                    player.Inventory[6] = player.Manager.GameData.Items[0xadc]; // t8 robe
                    player.Inventory[7] = player.Manager.GameData.Items[0xabd]; // Superior
                    player.UpdateCount++;
                }
            }
            player.SendInfo("Set Given");
            return true;
        }
    }
    internal class PriestCommand : Command
    {
        public PriestCommand() : base("priest", 1)
        {
        }
        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            for (int i = 0; i < player.Inventory.Length; i++)
            {
                if (player.Inventory[4] != null) { return false; }
                if (player.Inventory[5] != null) { return false; }
                if (player.Inventory[6] != null) { return false; }
                if (player.Inventory[7] != null) { return false; }
                else
                {
                    player.Inventory[4] = player.Manager.GameData.Items[0xa07]; // Wand of death
                    player.Inventory[5] = player.Manager.GameData.Items[0xa33]; // t4 tome
                    player.Inventory[6] = player.Manager.GameData.Items[0xadc]; // t8 robe
                    player.Inventory[7] = player.Manager.GameData.Items[0xabd]; // Superior
                    player.UpdateCount++;
                }
            }
            player.SendInfo("Set Given");
            return true;
        }
    }
    internal class WarriorCommand : Command
    {
        public WarriorCommand() : base("warrior", 1)
        {
        }
        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            for (int i = 0; i < player.Inventory.Length; i++)
            {
                if (player.Inventory[4] != null) { return false; }
                if (player.Inventory[5] != null) { return false; }
                if (player.Inventory[6] != null) { return false; }
                if (player.Inventory[7] != null) { return false; }
                else
                {
                    player.Inventory[4] = player.Manager.GameData.Items[0xa82]; // Ravenheart
                    player.Inventory[5] = player.Manager.GameData.Items[0xa6a]; // steel helm
                    player.Inventory[6] = player.Manager.GameData.Items[0xa12]; // mithril
                    player.Inventory[7] = player.Manager.GameData.Items[0xabd]; // Superior
                    player.UpdateCount++;
                }
            }
            player.SendInfo("Set Given");
            return true;
        }
    }
    internal class KnightCommand : Command
    {
        public KnightCommand() : base("knight", 1)
        {
        }
        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            for (int i = 0; i < player.Inventory.Length; i++)
            {
                if (player.Inventory[4] != null) { return false; }
                if (player.Inventory[5] != null) { return false; }
                if (player.Inventory[6] != null) { return false; }
                if (player.Inventory[7] != null) { return false; }
                else
                {
                    player.Inventory[4] = player.Manager.GameData.Items[0xa82]; // Ravenheart
                    player.Inventory[5] = player.Manager.GameData.Items[0xa0b]; // t4 shield
                    player.Inventory[6] = player.Manager.GameData.Items[0xa12]; // mithril
                    player.Inventory[7] = player.Manager.GameData.Items[0xabd]; // Superior
                    player.UpdateCount++;
                }
            }
            player.SendInfo("Set Given");
            return true;
        }
    }
    internal class PaladinCommand : Command
    {
        public PaladinCommand() : base("paladin", 1)
        {
        }
        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            for (int i = 0; i < player.Inventory.Length; i++)
            {
                if (player.Inventory[4] != null) { return false; }
                if (player.Inventory[5] != null) { return false; }
                if (player.Inventory[6] != null) { return false; }
                if (player.Inventory[7] != null) { return false; }
                else
                {
                    player.Inventory[4] = player.Manager.GameData.Items[0xa82]; // Ravenheart
                    player.Inventory[5] = player.Manager.GameData.Items[0xa54]; // divine
                    player.Inventory[6] = player.Manager.GameData.Items[0xa12]; // mithril
                    player.Inventory[7] = player.Manager.GameData.Items[0xabd]; // Superior
                    player.UpdateCount++;
                }
            }
            player.SendInfo("Set Given");
            return true;
        }
    }
    internal class AssassinCommand : Command
    {
        public AssassinCommand() : base("assassin", 1)
        {
        }
        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            for (int i = 0; i < player.Inventory.Length; i++)
            {
                if (player.Inventory[4] != null) { return false; }
                if (player.Inventory[5] != null) { return false; }
                if (player.Inventory[6] != null) { return false; }
                if (player.Inventory[7] != null) { return false; }
                else
                {
                    player.Inventory[4] = player.Manager.GameData.Items[0xa19]; // Fire dagger
                    player.Inventory[5] = player.Manager.GameData.Items[0xaa7]; // t4 poison
                    player.Inventory[6] = player.Manager.GameData.Items[0xa0e]; // Studded
                    player.Inventory[7] = player.Manager.GameData.Items[0xabd]; // Superior
                    player.UpdateCount++;
                }
            }
            player.SendInfo("Set Given");
            return true;
        }
    }
    internal class NecromancerCommand : Command
    {
        public NecromancerCommand() : base("necromancer", 1)
        {
        }
        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            for (int i = 0; i < player.Inventory.Length; i++)
            {
                if (player.Inventory[4] != null) { return false; }
                if (player.Inventory[5] != null) { return false; }
                if (player.Inventory[6] != null) { return false; }
                if (player.Inventory[7] != null) { return false; }
                else
                {
                    player.Inventory[4] = player.Manager.GameData.Items[0xa9f]; // horror
                    player.Inventory[5] = player.Manager.GameData.Items[0xaae]; // Lifedrinker
                    player.Inventory[6] = player.Manager.GameData.Items[0xadc]; // t8 robe
                    player.Inventory[7] = player.Manager.GameData.Items[0xabd]; // Superior
                    player.UpdateCount++;
                }
            }
            player.SendInfo("Set Given");
            return true;
        }
    }
    internal class HuntressCommand : Command
    {
        public HuntressCommand() : base("huntress", 1)
        {
        }
        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            for (int i = 0; i < player.Inventory.Length; i++)
            {
                if (player.Inventory[4] != null) { return false; }
                if (player.Inventory[5] != null) { return false; }
                if (player.Inventory[6] != null) { return false; }
                if (player.Inventory[7] != null) { return false; }
                else
                {
                    player.Inventory[4] = player.Manager.GameData.Items[0xa1e]; // Golden bow
                    player.Inventory[5] = player.Manager.GameData.Items[0xab5]; // Dstalker
                    player.Inventory[6] = player.Manager.GameData.Items[0xa0e]; // Studded
                    player.Inventory[7] = player.Manager.GameData.Items[0xabd]; // Superior
                    player.UpdateCount++;
                }
            }
            player.SendInfo("Set Given");
            return true;
        }
    }
    internal class MysticCommand : Command
    {
        public MysticCommand() : base("mystic", 1)
        {
        }
        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            for (int i = 0; i < player.Inventory.Length; i++)
            {
                if (player.Inventory[4] != null) { return false; }
                if (player.Inventory[5] != null) { return false; }
                if (player.Inventory[6] != null) { return false; }
                if (player.Inventory[7] != null) { return false; }
                else
                {
                    player.Inventory[4] = player.Manager.GameData.Items[0xa9f]; // horror
                    player.Inventory[5] = player.Manager.GameData.Items[0xa45]; // Banishment Orb
                    player.Inventory[6] = player.Manager.GameData.Items[0xadc]; // t8 robe
                    player.Inventory[7] = player.Manager.GameData.Items[0xabd]; // Superior
                    player.UpdateCount++;
                }
            }
            player.SendInfo("Set Given");
            return true;
        }
    }
    internal class TricksterCommand : Command
    {
        public TricksterCommand() : base("trickster", 1)
        {
        }
        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            for (int i = 0; i < player.Inventory.Length; i++)
            {
                if (player.Inventory[4] != null) { return false; }
                if (player.Inventory[5] != null) { return false; }
                if (player.Inventory[6] != null) { return false; }
                if (player.Inventory[7] != null) { return false; }
                else
                {
                    player.Inventory[4] = player.Manager.GameData.Items[0xa19]; // Fire dagger
                    player.Inventory[5] = player.Manager.GameData.Items[0xb1f]; // Phantoms
                    player.Inventory[6] = player.Manager.GameData.Items[0xa0e]; // Studded
                    player.Inventory[7] = player.Manager.GameData.Items[0xabd]; // Superior
                    player.UpdateCount++;
                }
            }
            player.SendInfo("Set Given");
            return true;
        }
    }
    internal class SorcererCommand : Command
    {
        public SorcererCommand() : base("sorcerer", 1)
        {
        }
        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            for (int i = 0; i < player.Inventory.Length; i++)
            {
                if (player.Inventory[4] != null) { return false; }
                if (player.Inventory[5] != null) { return false; }
                if (player.Inventory[6] != null) { return false; }
                if (player.Inventory[7] != null) { return false; }
                else
                {
                    player.Inventory[4] = player.Manager.GameData.Items[0xa07]; // Wand of death
                    player.Inventory[5] = player.Manager.GameData.Items[0xb31]; // Skybolt
                    player.Inventory[6] = player.Manager.GameData.Items[0xadc]; // t8 robe
                    player.Inventory[7] = player.Manager.GameData.Items[0xabd]; // Superior
                    player.UpdateCount++;
                }
            }
            player.SendInfo("Set Given");
            return true;
        }
    }
    internal class NinjaCommand : Command
    {
        public NinjaCommand() : base("ninja", 1)
        {
        }
        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            for (int i = 0; i < player.Inventory.Length; i++)
            {
                if (player.Inventory[4] != null) { return false; }
                if (player.Inventory[5] != null) { return false; }
                if (player.Inventory[6] != null) { return false; }
                if (player.Inventory[7] != null) { return false; }
                else
                {
                    player.Inventory[4] = player.Manager.GameData.Items[0xc4c]; // demon edge
                    player.Inventory[5] = player.Manager.GameData.Items[0xc57]; // Ice Star
                    player.Inventory[6] = player.Manager.GameData.Items[0xa0e]; // Studded
                    player.Inventory[7] = player.Manager.GameData.Items[0xabd]; // Superior
                    player.UpdateCount++;
                }
            }
            player.SendInfo("Set Given");
            return true;
        }

        internal class KickAllCommand : Command
        {
            public KickAllCommand() : base("kickall", 4)
            {
            }

            protected override bool Process(Player player, RealmTime time, string[] args)
            {
                int clients = 0;
                foreach (var i in player.Manager.Clients.Values.Where(i => i.Account.Name != player.Name))
                {
                    i.Disconnect();
                    clients++;
                }
                player.SendInfo($"Success, kicked {clients} clients");
                return true;
            }
        }

        internal class Take : Command
        {
            public Take()
                : base("take", 5)
            {
            }

            protected override bool Process(Player player, RealmTime time, string[] args)
            {
                if (args.Length < 2)
                {
                    player.SendHelp("Usage: /take <playername> <Itemname>");
                    return false;
                }
                Player p = player.Manager.FindPlayer(args[0]);
                string name = string.Join(" ", args.ToArray(), 1, (args.Length - 1)).Trim();
                ushort objType;
                Dictionary<string, ushort> icdatas = new Dictionary<string, ushort>(player.Manager.GameData.IdToObjectType,
                    StringComparer.OrdinalIgnoreCase);
                if (!icdatas.TryGetValue(name, out objType))
                {
                    player.SendError("Unknown type!");
                    return false;
                }
                if (!player.Manager.GameData.Items[objType].Secret || player.Client.Account.Rank >= 3)
                {
                    for (int i = 0; i < p.Inventory.Length; i++)
                        if (p.Inventory[i] == player.Manager.GameData.Items[objType])
                        {
                            p.Inventory[i] = null;
                            p.UpdateCount++;
                            p.SaveToCharacter();
                            player.SendInfo("You took " + p.Name + " " + name + " away from them!");
                            break;
                        }
                }
                else
                {
                    player.SendError("Item not found Or Item cannot be taken!");
                    return false;
                }
                return true;
            }
        }

        internal class IPCommand : Command
        {
            public IPCommand()
                : base("ip", 5)
            {
            }

            protected override bool Process(Player player, RealmTime time, string[] args)
            {
                var plr = player.Manager.FindPlayer(args[0]);
                var sb = new StringBuilder(plr.Name + "'s IP: ");
                sb.AppendFormat("{0}",
                    plr.Client.Socket.RemoteEndPoint);
                player.SendInfo(sb.ToString());
                return true;
            }
        }

        internal class KillPlayerCommand : Command
        {
            public KillPlayerCommand()
                : base("killplayer", 5)
            {
            }

            protected override bool Process(Player player, RealmTime time, string[] args)
            {
                foreach (Client i in player.Manager.Clients.Values)
                {
                    if (i.Account.Name.EqualsIgnoreCase(args[0]))
                    {
                        i.Player.HP = 0;
                        i.Player.Death("High Council");
                        player.SendInfo("Player has been killed!");
                        return true;
                    }
                }
                player.SendError(string.Format("Player '{0}' could not be found!", args));
                return false;
            }
        }

        internal class ServerQuitCommand : Command
        {
            public ServerQuitCommand()
                : base("squit", 8)
            {
            }
            protected override bool Process(Player player, RealmTime time, string[] args)
            {
                player.Client.SendPacket(new TextPacket
                {
                    BubbleTime = 0,
                    Stars = -1,
                    Name = "@INFO",
                    Text = "Server is turning off in 1 minute. Leave the server to prevent account in use!"
                });
                player.Owner.Timers.Add(new WorldTimer(30000, (world, t) =>
                {
                    player.Client.SendPacket(new TextPacket
                    {
                        BubbleTime = 0,
                        Stars = -1,
                        Name = "@INFO",
                        Text = "Server is turning off in 30 seconds. Leave the server to prevent account in use!"
                    });
                }));

                player.Owner.Timers.Add(new WorldTimer(45000, (world, t) =>
                {
                    player.Client.SendPacket(new TextPacket
                    {
                        BubbleTime = 0,
                        Stars = -1,
                        Name = "@INFO",
                        Text = "Server is turning off in 15 seconds. Leave the server to prevent account in use!"
                    });
                }));
                player.Owner.Timers.Add(new WorldTimer(55000, (world, t) =>
                {
                    player.Client.SendPacket(new TextPacket
                    {
                        BubbleTime = 0,
                        Stars = -1,
                        Name = "@INFO",
                        Text = "Server is turning off in 5 seconds. Leave the server to prevent account in use!"
                    });
                }));
                player.Owner.Timers.Add(new WorldTimer(60000, (world, t) =>
                {
                    Environment.Exit(0);
                }));
                return true;
            }
        }


    internal class BanCommand : Command
    {
        public BanCommand() :
            base("ban", 4)
        {
        }

        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            var p = player.Manager.FindPlayer(args[1]);
            if (p == null)
            {
                player.SendError("Player not found");
                return false;
            }
            player.Manager.Database.DoActionAsync(db =>
            {
                var cmd = db.CreateQuery();
                cmd.CommandText = "UPDATE accounts SET banned=1 WHERE id=@accId;";
                cmd.Parameters.AddWithValue("@accId", p.AccountId);
                cmd.ExecuteNonQuery();
            });
            return true;
        }
    }

    internal class SpawnCommand : Command
    {
        public SpawnCommand()
            : base("spawn", 5)
        {
        }


        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            int num;
            if (args.Length > 0 && int.TryParse(args[0], out num)) //multi
            {
                string name = string.Join(" ", args.Skip(1).ToArray());
                World w = player.Manager.GetWorld(player.Owner.Id); //can't use Owner here, as it goes out of scope
                string announce = "Spawning " + num + " of " + name + " in 3 seconds . . .";
                ushort objType;
                //creates a new case insensitive dictionary based on the XmlDatas
                Dictionary<string, ushort> icdatas = new Dictionary<string, ushort>(player.Manager.GameData.IdToObjectType, StringComparer.OrdinalIgnoreCase);
                if (name.ToLower() == "vault chest" && player.Client.Account.Rank < 9)
                {
                    player.SendInfo("You are not allowed to spawn this entity.");
                    return false;
                }
                if (!icdatas.TryGetValue(name, out objType) ||
                !player.Manager.GameData.ObjectDescs.ContainsKey(objType))
                {
                    player.SendInfo("Unknown entity!");
                    return false;
                }
                int c = int.Parse(args[0]);
                if (c > 50)
                {
                    player.SendInfo("Cant spawn that many enemies.");
                    return false;
                }
                w.BroadcastPacket(new NotificationPacket
                {
                    Color = new ARGB(0x00ff00),
                    ObjectId = player.Client.Player.Id,
                    Text = "{\"key\":\"blank\",\"tokens\":{\"data\":\"" + announce + "\"}}",
                }, null);
                player.Owner.Timers.Add(new WorldTimer(3000, (world, t) =>
                {
                    for (int i = 0; i < num; i++)
                    {
                        Entity entity = Entity.Resolve(player.Manager, objType);
                        if (player.Owner is Nexus)
                        {
                            entity.Move(113, 74);
                            player.Owner.EnterWorld(entity);
                        }
                        else
                        {
                            entity.Move(player.X, player.Y);
                            player.Owner.EnterWorld(entity);
                        }
                    }
                }));
                player.SendInfo("Success!");
            }
            else
            {
                string name = string.Join(" ", args);
                ushort objType;
                //creates a new case insensitive dictionary based on the XmlDatas
                Dictionary<string, ushort> icdatas = new Dictionary<string, ushort>(player.Manager.GameData.IdToObjectType, StringComparer.OrdinalIgnoreCase);
                if (name.ToLower() == "vault chest")
                {
                    player.SendInfo("You are not allowed to spawn this entity.");
                    return false;
                }
                if (!icdatas.TryGetValue(name, out objType) || !player.Manager.GameData.ObjectDescs.ContainsKey(objType))
                {
                    player.SendHelp("Usage: /spawn <entityname>");
                    return false;
                }
                World w = player.Manager.GetWorld(player.Owner.Id); //can't use Owner here, as it goes out of scope
                string announce = "Spawning " + name + " in 5 seconds . . .";
                w.BroadcastPacket(new NotificationPacket
                {
                    Color = new ARGB(0x00ff00),
                    ObjectId = player.Client.Player.Id,
                    Text = "{\"key\":\"blank\",\"tokens\":{\"data\":\"" + announce + "\"}}",
                }, null);
                player.Owner.Timers.Add(new WorldTimer(3000, (world, t) =>
                {
                    Entity entity = Entity.Resolve(player.Manager, objType);
                    if (player.Owner is Nexus)
                    {
                        entity.Move(113, 74);
                        player.Owner.EnterWorld(entity);
                    }
                    else
                    {
                        entity.Move(player.X, player.Y);
                        player.Owner.EnterWorld(entity);
                    }
                }));
            }
            return true;
        }
    }

    internal class GlandCommand : Command
    {
        public GlandCommand()
            : base("glands", 0)
        {
        }

        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            if (player.Owner is Nexus || player.Owner is PetYard || player.Owner is ClothBazaar || player.Owner is Vault || player.Owner is GuildHall)
            {
                player.SendInfo("You cannot use this command here");
                return false;
            }
            string[] random = "950|960|970|980|990|1000|1010|1020|1030|1040|1050|1060|1070|1080|1090|1100|1100|1110|1120|1130|1140|1050".Split('|');
            int tplocation = new Random().Next(random.Length);
            string topdank = random[tplocation];
            int x, y;
            try
            {
                x = int.Parse(topdank);
                y = int.Parse(topdank);
            }
            catch
            {
                player.SendError("Invalid coordinates!");
                return false;
            }
            player.Move(x + 0.5f, y + 0.5f);
            if (player.Pet != null)
                player.Pet.Move(x + 0.5f, y + 0.5f);
            player.UpdateCount++;
            player.Owner.BroadcastPacket(new GotoPacket
            {
                ObjectId = player.Id,
                Position = new Position
                {
                    X = player.X,
                    Y = player.Y
                }
            }, null);
            player.ApplyConditionEffect(new ConditionEffect()
            {
                Effect = ConditionEffectIndex.Invulnerable,
                DurationMS = 2500,
            });
            player.ApplyConditionEffect(new ConditionEffect()
            {
                Effect = ConditionEffectIndex.Invisible,
                DurationMS = 2500,
            });
            return true;
        }
    }

    internal class PetMaxCommand : Command
    {
        public PetMaxCommand()
            : base("petmax", 4)
        {
        }

        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            if (player.Pet == null) return false;
            player.Pet.Feed(new PetFoodNomNomNom());
            player.Pet.UpdateCount++;
            return true;
        }

        private class PetFoodNomNomNom : IFeedable
        {
            public int FeedPower { get; set; }

            public PetFoodNomNomNom()
            {
                FeedPower = Int32.MaxValue;
            }
        }
    }


    internal class RankC2ommand : Command
    {
        public RankC2ommand()
            : base("rank", 5)
        {
        }

        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            if (string.IsNullOrEmpty(args[0]))
            {
                player.SendInfo("Usage: /rank <playername> <number>");
                player.SendInfo("Owner: 5");
                player.SendInfo("Staff: 4");
                player.SendInfo("Patron: 3");
                player.SendInfo("Supporter: 2");
                player.SendInfo("Donor: 1");
                return false;
            }
            int c = int.Parse(args[1]);
            if (c > 4)
            {
                player.SendHelp("You cannot rank someone to 10. This must be done in the database.");
                return false;
            }
            if (c > 5 && player.Client.Account.Rank == 5)
            {
                player.SendHelp("At least you tried...");
                return false;
            }
            var p = player.Manager.FindPlayer(args[0]);
            player.Manager.Database.DoActionAsync(db =>
            {
                var cmd = db.CreateQuery();
                cmd.CommandText = "UPDATE accounts SET rank=@wutface WHERE id=@accId;";
                cmd.Parameters.AddWithValue("@accId", p.AccountId);
                cmd.Parameters.AddWithValue("@wutface", args[1]);
                player.SendInfo("Successfully ranked " + p.Name);
                p.SendInfo("You have been ranked by " + player.Name);
                cmd.ExecuteNonQuery();
                db.Dispose();
            });
            return true;
        }
    }

    internal class GiftCommand : Command
    {
        public GiftCommand()
            : base("gift", 5)
        {
        }
        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            if (args.Length == 1)
            {
                player.SendHelp("Usage: /gift <Playername> <Itemname>");
                return false;
            }
            string name = string.Join(" ", args.Skip(1).ToArray()).Trim();
            var plr = player.Manager.FindPlayer(args[0]);
            ushort objType;
            Dictionary<string, ushort> icdatas = new Dictionary<string, ushort>(player.Manager.GameData.IdToObjectType,
                StringComparer.OrdinalIgnoreCase);
            if (!icdatas.TryGetValue(name, out objType))
            {
                player.SendError("Item not found, perhaps a spelling error?");
                return false;
            }
            if (!player.Manager.GameData.Items[objType].Secret || player.Client.Account.Rank >= 5)
            {
                for (int i = 0; i < plr.Inventory.Length; i++)
                    if (plr.Inventory[i] == null)
                    {
                        plr.Inventory[i] = player.Manager.GameData.Items[objType];
                        plr.UpdateCount++;
                        plr.SaveToCharacter();
                        player.SendInfo("Success sending " + name + " to " + plr.Name);
                        plr.SendInfo("You got a " + name + " from " + player.Name);
                        break;
                    }
            }
            else
            {
                player.SendError("Item failed sending to " + plr.Name + ", make sure you spelt the command right, and their name!");
                return false;
            }
            return true;
        }
    }

    internal class AddEffCommand : Command
    {
        public AddEffCommand()
            : base("addeff", 1)
        {
        }

        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            if (args.Length == 0)
            {
                player.SendHelp("Usage: /addeff <Effectname or Effectnumber>");
                return false;
            }
            try
            {
                player.ApplyConditionEffect(new ConditionEffect
                {
                    Effect = (ConditionEffectIndex)Enum.Parse(typeof(ConditionEffectIndex), args[0].Trim(), true),
                    DurationMS = -1
                });
                {
                    player.SendInfo("Success!");
                }
            }
            catch
            {
                player.SendError("Invalid effect!");
                return false;
            }
            return true;
        }
    }

    internal class RemoveEffCommand : Command
    {
        public RemoveEffCommand()
            : base("remeff", 1)
        {
        }

        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            if (args.Length == 0)
            {
                player.SendHelp("Usage: /remeff <Effectname or Effectnumber>");
                return false;
            }
            try
            {
                player.ApplyConditionEffect(new ConditionEffect
                {
                    Effect = (ConditionEffectIndex)Enum.Parse(typeof(ConditionEffectIndex), args[0].Trim(), true),
                    DurationMS = 0
                });
                player.SendInfo("Success!");
            }
            catch
            {
                player.SendError("Invalid effect!");
                return false;
            }
            return true;
        }
    }

    internal class GiveCommand : Command
    {
        public GiveCommand()
            : base("give", 2)
        {
        }

        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            if (args.Length == 0)
            {
                player.SendHelp("Usage: /give <Itemname>");
                return false;
            }
            string name = string.Join(" ", args.ToArray()).Trim();
            ushort objType;
            //creates a new case insensitive dictionary based on the XmlDatas
            Dictionary<string, ushort> icdatas = new Dictionary<string, ushort>(player.Manager.GameData.IdToObjectType,
                StringComparer.OrdinalIgnoreCase);
            if (!icdatas.TryGetValue(name, out objType))
            {
                player.SendError("Unknown type!");
                return false;
            }
            if (!player.Manager.GameData.Items[objType].Secret || player.Client.Account.Rank >= 4)
            {
                for (int i = 0; i < player.Inventory.Length; i++)
                    if (player.Inventory[i] == null)
                    {
                        player.Inventory[i] = player.Manager.GameData.Items[objType];
                        player.UpdateCount++;
                        player.SaveToCharacter();
                        player.SendInfo("Success!");
                        break;
                    }
            }
            else
            {
                player.SendError("Item cannot be given!");
                return false;
            }
            return true;
        }
    }

    internal class TpCommand : Command
    {
        public TpCommand()
            : base("tp", 1)
        {
        }

        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            if (args.Length == 0 || args.Length == 1)
            {
                player.SendHelp("Usage: /tp <X coordinate> <Y coordinate>");
            }
            else
            {
                int x, y;
                try
                {
                    x = int.Parse(args[0]);
                    y = int.Parse(args[1]);
                }
                catch
                {
                    player.SendError("Invalid coordinates!");
                    return false;
                }
                player.Move(x + 0.5f, y + 0.5f);
                if (player.Pet != null)
                    player.Pet.Move(x + 0.5f, y + 0.5f);
                player.UpdateCount++;
                player.Owner.BroadcastPacket(new GotoPacket
                {
                    ObjectId = player.Id,
                    Position = new Position
                    {
                        X = player.X,
                        Y = player.Y
                    }
                }, null);
            }
            return true;
        }
    }

    class KillAll : Command
    {
        public KillAll() : base("killAll", 4) { }

        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            var iterations = 0;
            var lastKilled = -1;
            var killed = 0;

            var mobName = args.Aggregate((s, a) => string.Concat(s, " ", a));
            while (killed != lastKilled)
            {
                lastKilled = killed;
                foreach (var i in player.Owner.Enemies.Values.Where(e =>
                    e.ObjectDesc?.ObjectId != null && e.ObjectDesc.ObjectId.ContainsIgnoreCase(mobName)))
                {
                    i.Death(time);
                    killed++;
                }
                if (++iterations >= 5)
                    break;
            }

            player.SendInfo($"{killed} enemy killed!");
            return true;
        }
    }

    internal class Kick : Command
    {
        public Kick()
            : base("kick", 4)
        {
        }

        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            if (args.Length == 0)
            {
                player.SendHelp("Usage: /kick <playername>");
                return false;
            }
            try
            {
                foreach (KeyValuePair<int, Player> i in player.Owner.Players)
                {
                    if (i.Value.Name.ToLower() == args[0].ToLower().Trim())
                    {
                        player.SendInfo("Player Disconnected");
                        i.Value.Client.Disconnect();
                    }
                }
            }
            catch
            {
                player.SendError("Cannot kick!");
                return false;
            }
            return true;
        }
    }

    internal class Mute : Command
    {
        public Mute()
            : base("mute", 4)
        {
        }

        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            if (args.Length == 0)
            {
                player.SendHelp("Usage: /mute <playername>");
                return false;
            }
            try
            {
                foreach (KeyValuePair<int, Player> i in player.Owner.Players)
                {
                    if (i.Value.Name.ToLower() == args[0].ToLower().Trim())
                    {
                        i.Value.Muted = true;
                        i.Value.Manager.Database.DoActionAsync(db => db.MuteAccount(i.Value.AccountId));
                        player.SendInfo("Player Muted.");
                    }
                }
            }
            catch
            {
                player.SendError("Cannot mute!");
                return false;
            }
            return true;
        }
    }

    internal class Max : Command
    {
        public Max()
            : base("max", 4)
        {
        }

        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            try
            {
                player.Stats[0] = player.ObjectDesc.MaxHitPoints;
                player.Stats[1] = player.ObjectDesc.MaxMagicPoints;
                player.Stats[2] = player.ObjectDesc.MaxAttack;
                player.Stats[3] = player.ObjectDesc.MaxDefense;
                player.Stats[4] = player.ObjectDesc.MaxSpeed;
                player.Stats[5] = player.ObjectDesc.MaxHpRegen;
                player.Stats[6] = player.ObjectDesc.MaxMpRegen;
                player.Stats[7] = player.ObjectDesc.MaxDexterity;
                player.SaveToCharacter();
                player.Client.Save();
                player.UpdateCount++;
                player.SendInfo("Successfully maxed your characters.");
            }
            catch
            {
                player.SendError("Error while maxing stats");
                return false;
            }
            return true;
        }
    }

    internal class UnMute : Command
    {
        public UnMute()
            : base("unmute", 4)
        {
        }

        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            if (args.Length == 0)
            {
                player.SendHelp("Usage: /unmute <playername>");
                return false;
            }
            try
            {
                foreach (KeyValuePair<int, Player> i in player.Owner.Players)
                {
                    if (i.Value.Name.ToLower() == args[0].ToLower().Trim())
                    {
                        i.Value.Muted = true;
                        i.Value.Manager.Database.DoActionAsync(db => db.UnmuteAccount(i.Value.AccountId));
                        player.SendInfo("Player Unmuted.");
                    }
                }
            }
            catch
            {
                player.SendError("Cannot unmute!");
                return false;
            }
            return true;
        }
    }
    internal class SWhoCommand : Command //get all players from all worlds (this may become too large!)
    {
        public SWhoCommand()
            : base("swho", 0)
        {
        }

        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            StringBuilder sb = new StringBuilder("All players online: ");

            foreach (KeyValuePair<int, World> w in player.Manager.Worlds)
            {
                World world = w.Value;
                if (w.Key != 0)
                {
                    Player[] copy = world.Players.Values.ToArray();
                    if (copy.Length != 0)
                    {
                        for (int i = 0; i < copy.Length; i++)
                        {
                            sb.Append(copy[i].Name);
                            sb.Append(", ");
                        }
                    }
                }
            }
            string fixedString = sb.ToString().TrimEnd(',', ' ');

            player.SendInfo(fixedString);
            return true;
        }
    }

    internal class Announcement : Command
    {
        public Announcement()
            : base("announce", 4)
        {
        }

        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            if (args.Length == 0)
            {
                player.SendHelp("Usage: /announce <saytext>");
                return false;
            }
            string saytext = string.Join(" ", args);

            foreach (Client i in player.Manager.Clients.Values)
            {
                i.SendPacket(new TextPacket
                {
                    BubbleTime = 0,
                    Stars = -1,
                    Name = "@Server Announcement",
                    Text = " " + saytext
                });
            }
            return true;
        }
    }

    internal class DonorSpawnCommand : Command
    {
        public DonorSpawnCommand()
            : base("dspawn", 3)
        {
        }

        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            if (player.Owner.Name != "Vault" && player.Client.Account.Rank <= 5)
            {
                player.SendError("You must be in your vault to spawn.");
                return false;
            }
            int num;
            if (args.Length > 0 && int.TryParse(args[0], out num)) //multi
            {
                string name = string.Join(" ", args.Skip(1).ToArray());
                World w = player.Manager.GetWorld(player.Owner.Id); //can't use Owner here, as it goes out of scope
                string announce = "Spawning " + num + " of " + name + " in 2.5 seconds . . .";
                ushort objType;
                //creates a new case insensitive dictionary based on the XmlDatas
                Dictionary<string, ushort> icdatas = new Dictionary<string, ushort>(player.Manager.GameData.IdToObjectType, StringComparer.OrdinalIgnoreCase);
                if (name.ToLower() == "vault chest" && player.Client.Account.Rank < 1)
                {
                    player.SendInfo("You are not allowed to spawn this entity.");
                    return false;
                }
                if (!icdatas.TryGetValue(name, out objType) ||
                !player.Manager.GameData.ObjectDescs.ContainsKey(objType))
                {
                    player.SendInfo("Unknown entity!");
                    return false;
                }
                int c = int.Parse(args[0]);
                if (c > 1)
                {
                    player.SendInfo("Cant spawn that many enemies. The maximum limit is only 1.");
                    return false;
                }
                w.BroadcastPacket(new NotificationPacket
                {
                    Color = new ARGB(0x00ff00),
                    ObjectId = player.Client.Player.Id,
                    Text = "{\"key\":\"blank\",\"tokens\":{\"data\":\"" + announce + "\"}}",
                }, null);
                player.Owner.Timers.Add(new WorldTimer(2500, (world, t) =>
                {
                    for (int i = 0; i < num; i++)
                    {
                        Entity entity = Entity.Resolve(player.Manager, objType);
                        if (player.Owner is Nexus)
                        {
                            entity.Move(1, 1);
                            player.Owner.EnterWorld(entity);
                        }
                        else
                        {
                            entity.Move(player.X, player.Y);
                            player.Owner.EnterWorld(entity);
                        }
                    }
                }));
                player.SendInfo("Success!");
            }
            else
            {
                string name = string.Join(" ", args);
                ushort objType;
                //creates a new case insensitive dictionary based on the XmlDatas
                Dictionary<string, ushort> icdatas = new Dictionary<string, ushort>(player.Manager.GameData.IdToObjectType, StringComparer.OrdinalIgnoreCase);
                if (name.ToLower() == "vault chest")
                {
                    player.SendInfo("You are not allowed to spawn this entity.");
                    return false;
                }
                if (!icdatas.TryGetValue(name, out objType) || !player.Manager.GameData.ObjectDescs.ContainsKey(objType))
                {
                    player.SendHelp("Usage: /spawn <entityname>");
                    return false;
                }
                World w = player.Manager.GetWorld(player.Owner.Id); //can't use Owner here, as it goes out of scope
                string announce = "Spawning " + name + " in 5 seconds . . .";
                w.BroadcastPacket(new NotificationPacket
                {
                    Color = new ARGB(0x00ff00),
                    ObjectId = player.Client.Player.Id,
                    Text = "{\"key\":\"blank\",\"tokens\":{\"data\":\"" + announce + "\"}}",
                }, null);
                player.Owner.Timers.Add(new WorldTimer(5000, (world, t) =>
                {
                    Entity entity = Entity.Resolve(player.Manager, objType);
                    if (player.Owner is Nexus)
                    {
                        entity.Move(1, 1);
                        player.Owner.EnterWorld(entity);
                    }
                    else
                    {
                        entity.Move(player.X, player.Y);
                        player.Owner.EnterWorld(entity);
                    }
                }));
            }
            return true;
        }
    }

    internal class SuicideCommand : Command
    {
        public SuicideCommand()
            : base("suicide")
        {
        }

        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            if (!(player.Owner is Nexus || player.Owner is Vault))
            {
                player.SendInfo("You cannot commit suicide in the nexus or guild hall.");
                return false;
            }
            else
            {
                player.HP = 0;
                player.Death("Bleach");
                return true;
            }
        }
    }

    internal class Summon : Command
    {
        public Summon()
            : base("summon", 5)
        {
        }

        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            if (player.Owner is Vault || player.Owner is PetYard)
            {
                player.SendInfo("You cant summon in this world.");
                return false;
            }
            foreach (KeyValuePair<string, Client> i in player.Manager.Clients)
            {
                if (i.Value.Player.Name.EqualsIgnoreCase(args[0]))
                {
                    Packet pkt;
                    if (i.Value.Player.Owner == player.Owner)
                    {
                        i.Value.Player.Move(player.X, player.Y);
                        pkt = new GotoPacket
                        {
                            ObjectId = i.Value.Player.Id,
                            Position = new Position(player.X, player.Y)
                        };
                        i.Value.Player.UpdateCount++;
                        player.SendInfo("Player summoned!");
                    }
                    else
                    {
                        pkt = new ReconnectPacket
                        {
                            GameId = player.Owner.Id,
                            Host = "",
                            IsFromArena = false,
                            Key = player.Owner.PortalKey,
                            KeyTime = -1,
                            Name = player.Owner.Name,
                            Port = -1
                        };
                        player.SendInfo("Player will connect to you now!");
                    }

                    i.Value.SendPacket(pkt);

                    return true;
                }
            }
            player.SendError(string.Format("Player '{0}' could not be found!", args));
            return false;
        }
    }

    internal class RestartCommand : Command
    {
        public RestartCommand()
            : base("restart", 5)
        {
        }

        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            try
            {
                foreach (KeyValuePair<int, World> w in player.Manager.Worlds)
                {
                    World world = w.Value;
                    if (w.Key != 0)
                    {
                        world.BroadcastPacket(new TextPacket
                        {
                            Name = "@ANNOUNCEMENT",
                            Stars = -1,
                            BubbleTime = 0,
                            Text =
                                "Server restarting soon. Please be ready to disconnect. Estimated server down time: 30 Seconds - 1 Week"
                        }, null);
                    }
                }
            }
            catch
            {
                player.SendError("Cannot say that in announcement!");
                return false;
            }
            return true;
        }
    }

    internal class TqCommand : Command
    {
        public TqCommand()
            : base("tq", 1)
        {
        }

        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            if (player.Quest == null)
            {
                player.SendError("Player does not have a quest!");
                return false;
            }
            player.Move(player.Quest.X + 2f, player.Quest.Y + 2f);
            if (player.Pet != null)
                player.Pet.Move(player.Quest.X + 2f, player.Quest.Y + 2f);
            player.UpdateCount++;
            player.Owner.BroadcastPacket(new GotoPacket
            {
                ObjectId = player.Id,
                Position = new Position
                {
                    X = player.Quest.X,
                    Y = player.Quest.Y
                }
            }, null);
            player.SendInfo("Success!");
            return true;
        }
    }

    internal class LevelCommand : Command
    {
        public LevelCommand()
            : base("level", 2)
        {
        }

        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            try
            {
                if (args.Length == 0)
                {
                    player.SendHelp("Use /level <ammount>");
                    return false;
                }
                if (args.Length == 1)
                {
                    player.Client.Character.Level = int.Parse(args[0]);
                    player.Client.Player.Level = int.Parse(args[0]);
                    player.UpdateCount++;
                    player.SendInfo("Success!");
                }
            }
            catch
            {
                player.SendError("Error setting your level!");
                return false;
            }
            return true;
        }
    }

    internal class SetCommand : Command
    {
        public SetCommand()
            : base("setStat", 5)
        {
        }

        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            if (args.Length == 2)
            {
                try
                {
                    string stat = args[0].ToLower();
                    int amount = int.Parse(args[1]);
                    switch (stat)
                    {
                        case "health":
                        case "hp":
                            player.Stats[0] = amount;
                            break;
                        case "mana":
                        case "mp":
                            player.Stats[1] = amount;
                            break;
                        case "atk":
                        case "att":
                        case "attack":
                            player.Stats[2] = amount;
                            break;
                        case "def":
                        case "defense":
                            player.Stats[3] = amount;
                            break;
                        case "spd":
                        case "speed":
                            player.Stats[4] = amount;
                            break;
                        case "vit":
                        case "vitality":
                            player.Stats[5] = amount;
                            break;
                        case "wis":
                        case "wisdom":
                            player.Stats[6] = amount;
                            break;
                        case "dex":
                        case "dexterity":
                            player.Stats[7] = amount;
                            break;
                        default:
                            player.SendError("Invalid Stat");
                            player.SendHelp("Stats: Health, Mana, Attack, Defense, Speed, Vitality, Wisdom, Dexterity");
                            player.SendHelp("Shortcuts: Hp, Mp, Atk, Att, Def, Spd, Vit, Wis, Dex");
                            return false;
                    }
                    player.SaveToCharacter();
                    player.Client.Save();
                    player.UpdateCount++;
                    player.SendInfo("Success");
                }
                catch
                {
                    player.SendError("Error while setting stat. Make sure you have wrote the correct stat name. Otherwise, you are using a number that is too large.");
                    return false;
                }
                return true;
            }
            else if (args.Length == 3)
            {
                foreach (Client i in player.Manager.Clients.Values)
                {
                    if (i.Account.Name.EqualsIgnoreCase(args[0]))
                    {
                        try
                        {
                            string stat = args[1].ToLower();
                            int amount = int.Parse(args[2]);
                            switch (stat)
                            {
                                case "health":
                                case "hp":
                                    i.Player.Stats[0] = amount;
                                    break;
                                case "mana":
                                case "mp":
                                    i.Player.Stats[1] = amount;
                                    break;
                                case "atk":
                                case "attack":
                                    i.Player.Stats[2] = amount;
                                    break;
                                case "def":
                                case "defence":
                                    i.Player.Stats[3] = amount;
                                    break;
                                case "spd":
                                case "speed":
                                    i.Player.Stats[4] = amount;
                                    break;
                                case "vit":
                                case "vitality":
                                    i.Player.Stats[5] = amount;
                                    break;
                                case "wis":
                                case "wisdom":
                                    i.Player.Stats[6] = amount;
                                    break;
                                case "dex":
                                case "dexterity":
                                    i.Player.Stats[7] = amount;
                                    break;
                                default:
                                    player.SendError("Invalid Stat");
                                    player.SendHelp("Stats: Health, Mana, Attack, Defence, Speed, Vitality, Wisdom, Dexterity");
                                    player.SendHelp("Shortcuts: Hp, Mp, Atk, Def, Spd, Vit, Wis, Dex");
                                    return false;
                            }
                            i.Player.SaveToCharacter();
                            i.Player.Client.Save();
                            i.Player.UpdateCount++;
                            player.SendInfo("Success");
                        }
                        catch
                        {
                            player.SendError("Error while setting stat");
                            return false;
                        }
                        return true;
                    }
                }
                player.SendError(string.Format("Player '{0}' could not be found!", args));
                return false;
            }
            else
            {
                player.SendHelp("Usage: /setStat <Stat> <Amount>");
                player.SendHelp("or");
                player.SendHelp("Usage: /setStat <Player> <Stat> <Amount>");
                player.SendHelp("Shortcuts: Hp, Mp, Atk, Def, Spd, Vit, Wis, Dex");
                return false;
            }
        }
    }

    internal class ListCommands : Command
    {
        public ListCommands() : base("commands", 0) { }

        protected override bool Process(Player player, RealmTime time, string[] args)
        {
            Dictionary<string, Command> cmds = new Dictionary<string, Command>();
            Type t = typeof(Command);
            foreach (Type i in t.Assembly.GetTypes())
                if (t.IsAssignableFrom(i) && i != t)
                {
                    Command instance = (Command)Activator.CreateInstance(i);
                    cmds.Add(instance.CommandName, instance);
                }
            StringBuilder sb = new StringBuilder("");
            Command[] copy = cmds.Values.ToArray();
            for (int i = 0; i < copy.Length; i++)
            {
                if (i != 0) sb.Append(" | ");
                sb.Append(copy[i].CommandName);
            }

            player.SendInfo(sb.ToString());
            return true;
        }
    }
}
}
