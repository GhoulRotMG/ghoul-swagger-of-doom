# Introduction and Credits

This project is intended for server owners to use a well-built FSoD source, that is fully functioning. The main intention is to make this server similar to the production source, in terms of mobs and behaviours, and also the general gameplay features.

This project is created by Infernape RotMG

This source is free to use, however, it would be appreciated if you could credit these guys:

- Fabiano-Swaggger-Of-Doom creators (Leaking the source/creating the features)
- Infernape RotMG/GhoulRotMG (Creating this project and guide)
- Wildshadow/Kabam/Deca Games (Developing the game)
- Winters Q (Base of the AS3 Tutorial)
- Everyone on MPGH/any other site that has contributed in any way.

# Instructions on how to edit the server and play:

**Requirements to run a RotMG Private Server:**

- An Linux/Windows operating system:
  - [Linux OS](https://www.ubuntu.com/download)
  - [Windows OS](https://www.microsoft.com/en-au/software-download/windows10)
- [Microsoft Visual Studio](https://www.visualstudio.com/downloads/)
- [XAMPP](https://www.apachefriends.org/download.html)
- [Heidi SQL](https://www.heidisql.com/download.php)
- [Sequel Pro: Mac SQL Manager](https://sequelpro.com/download)
- [LogMeIn Hamachi](https://www.vpn.net/)

### Notes:

**Choosing the right VPS**
You can use a VPS (Virtual Private Server) to run your server. A Virtual Private Server (VPS) is a virtual machine sold as a service by an Internet hosting service. A VPS runs its own copy of an operating system (OS). This is optimal because this VPS runs 24/7, and functions much better than your own PC. There are downsides, however, overall it is positive. It is recommended you use a Ubuntu or Windows VPS.
Cheap and Powerful VPS Providers:
- [Virmach](https://virmach.com/)
- [V2 Internet](https://cp.v2internet.com/)
- [OVH](https://www.ovh.com.au/)

Obviously, there are others, however these are the ones I recommend if you are a beginner owner.
- For the database management, I have suggested that you use HeidiSQl. However, you can feel free to use any other SQL management app, or even XAMPP itself. It is strongly recommended that you use [Redis](https://redis.io/), however, this is only recommended for people who aren't lazy.

**Do you want to know how to implement the changes that you have made to your server?**
- Make sure the first time that you do anything with the source, you build it.
- Any following times, you want to rebuild the solution.
- After rebuilding, it should come up with 0 errors. Then only does it mean that your server is ready to start.
- Launch wServer.exe and Server.exe from the big/debug folder.
- If everything opens up fine, and no errors come up, then you have passed the initial coding step.
- Go test your new feature. This is very important, as you want to make sure the things you added actually work in the server.
- If everything is fine, then you are good to go.

**Knowledge Required**
- Yes, I have created this, but this there is a limit on how far this tutorial can take you.
- You should have basic understanding of `C#`, the mildly difficult coding language that this project is created in. Go to [Learn C#](http://www.learncs.org/) if you want to learn more about `C#` coding.
- You must have time to put into your project, this isn't just a 5 minute thing.
- You must have a brain, and know how to use the provided information to expand on the possibilities.
- You must have basic technology access/Discord to contact me.
- My Discord name is: `GhoulRotMG#0001`. Please contact me if you need any help.

### Setting up the server configuration:

1) Change line 24: `svr0Adr:127.0.0.1` to your own Hamachi IP/VPS IP/Home IP. To find your IP, simply open your Hamachi app, or just Google search: `ip`.
2) You can also change line 20 `#svr0Name:Testing Server` To your server name - Change the "Testing Server". This will change the server name in the server selection menu.
3) Line 2 changes the port, however, I suggest that you do not change it.
4) You can also change line 22 `#svr0Location:Arlington, Virginia, USA` to you own location, just follow the same format.

### Setting up the MYSQL:

1) Open XAMPP Studio, that you have previously installed.
2) Make sure it is set up correctly.
3) You want to press start only the MYSQL. You don't need to start Apache. (There is a reason which you don't need to know).
4) You can go to settings, and change the ports and select if you want to auto start it upon system restart. However, it is recommended that you do not mess around with the ports.
5) You are now ready to launch and use the database.

### Setting up the database:

1) Install the database management app that you choose. I recommend using HeidiSQL.
2) Navigate to db/rotmgprod.sql.You want to right click, and open it with the software you have chosen.
3) When you first open HeidiSQL upon system restart, you are told to choose a session. When you first start, you should press the button named new in bottom left. Then, simply give it a name. Everything else should be left un-touched.
4) There should be a button somewhere to execute your sql. For HeidiSQL, it should be a green button near the middle of the top bar. **You may choose to proceed from here straight to the next section, but I recommend that you continue to step 5.**
5) Now, the database is ready. However, you do want to add the mystery boxes and the Alchemist.
6) For mystery boxes, you want to go to the box where you can run scripts in your SQL management software. For HeidiSQL, it is in the Query section below the bar with all the buttons. Delete everything in there. Then, go to this [Hastebin](https://hastebin.com/ewexixijet.sql) and copy all the contents. Now, you want to go to the Query, and paste in everything.
7) For the Alchemist, you want to go to the box where you can run scripts in your SQL management software. For HeidiSQL, it is in the Query section below the bar with all the buttons. Delete everything in there. Then, go to this [Hastebin](https://hastebin.com/ezikakatel.sql) and copy all the contents. Now, you want to go to the Query, an paste everything in.
9) Now, to implement the server into the server, you want to go to the final column in the respective section. Locate the column called "End Time". You want to set the time to somewhere in the next 3-4 years. So maybe like this: `2020-06-22 23:59:00`.
8) Obviously, following this format, you can customise your boxes. Follow my steps below to learn how to create your own packages.

#### Setting up custom packages for mystery box/alchemist:

1) Before you start creating your own package, it is important you understand the creating of the current ones. All items in the database are not represented as hex, but as decimals. You can use [this](https://www.rapidtables.com/convert/number/hex-to-decimal.html) converter to easily change your hex to decimal, or you can do it manually if you are a try hard.
2) To set up your custom package, you will need to set a few things. These include an type (just a number), a title (name of the package), an image (this is what it will display when you hover over the box). I have provided a PSD template to make your own graphics [here](https://mega.nz/#!zewDHQRK!PMxAR7HOERc6qOIlYG6yxZd8HYag7H7IEkz7BHZHYfU). ``Quick note: You must have Adobe Photoshop installed to open this file. Please just google a torrent/crack if you cannot afford it.``
3) Then, you will need to decide your contents. I suggest that you make a list of your contents, in hex form. Then, change all of them to decimal form, separated by commas. To created a jackpot, you want to make a list of items using commas (this will be the contents of the jackpot), then you put a `;` at the end to declare it as a group.
4) Next, you need to set the price of the box. For Alchemist, you will set the price in gold of the first run, then the second run, and same with tokens. For mystery boxes, you will need to identify the price type of the currency. Fame is 1, and Gold is 0.
5) After you do this, you should be ready to start your mystery box. If you have any issues, please contact me on Discord: `GhoulRotMG#0001`.

### Making it so that others can connect to your server:
1) To make sure others can connect to your server, you need to ensure the following points:
  - You need to make sure your firewall is **off**. This is important, because if it is on, it will block other people from connecting to your server.
  - You need to make sure your IP is correctly inputted into your SWF and Server.cfg.
  - You need to confirm that your ports are forwarded correctly.
  - You need to make sure your Hamachi is on, and Hamachi rooms are added.
  - You need to make sure any apps already taking up ports, such as Port 80 and Port Port 2050 are closed/deleted. One common app is Skype. To check for which ports are being used, simply open command prompt, and type in `netstat -anb`. This will show the ports being used, and you can close the applications.
2) Then, you want to set up your port forwarding (This does not apply to Hamachi servers).
  - To learn how to port forward on your device, visit:
  	- [Windows 10: ](https://www.youtube.com/watch?v=t06-xoB9DsE&ab_channel=RTOCTA)
  	- [Linux: ](https://www.youtube.com/watch?v=ndP0PvEX2uk)
  	- [Net Gear: ](https://www.noip.com/support/knowledgebase/port-forwarding-on-the-netgear-wgr614-v6-router/)

 
## Congrats, your server is now ready to start. You may now start it, and play by yourself or share it with others.

# Custom Features tutorial

To create a successful server, you will need to have some sort of custom aspect that will make it that bit better over others. Here, i'll cover how to add custom items/mobs/features to your server, to make it interesting.

## Custom Items Tutorial:

Creating custom items is one of the most important features of a private server. Everyone loves well coded, and aesthetically pleasing sprites on their favourite character, so i'll being teaching you the server-sided steps required to creating your first custom item.
### Weapon XML

Let me start by defining a few terms.
- ID: This is the name of your item.
- Type: This is the index of your item.
- RemoteTexture: Texture that goes from your server files to the user. Not added in client. **Do not use this.**
- ObjectID: This is commonly used for your projectile. This is simply the name of the projectile.
- DisplayID: This is the name that the client will display when you hover over the item in game.

**Now that you know this, lets begin.**

1) Create your own XML. A XML is `Extensible Markup Language` that is used in this game to make custom items in this game. To create them, you must have some knowledge of XMLS. Go to [this](https://www.w3schools.com/xml/) site if you want to build your knowledge.
2) You can use [Pfiffel](http://www.pfiffel.com/dps/) to make weapon XMLs, but there are restrictions. You will only be able to make items from projectiles/data that is already in the game.
3) When you finish making your XML, press Export XML, and it will copy it to your clipboard. Then, you want to paste it into your dat1.xml, or any file that you store your custom XMLs in. Then, begins the process of editing it to suit the server.
4) First of all, when you export your XML, the first thing you want to change your type of your item. This type is used by your server to recognise items. So, it goes without saying that you want to have this a unique one. To select an type, you have to just type `0x` and then make a few numbers. For example, `0x423`. However, there are a few rules you need to follow.
- You need to make sure your type is unique, simply press Control/Command + F, and search for it. If it comes up with no results, then you are good to go.
- You want to make your type is as low as possible. This is because if its too high, then it will lag your server. Don't ask questions why, just trust me on this.
- If you want to make a string of custom of custom items, it is best if you make your types in a row. For example, `0x423, 0x424, 0x425`, and etc. This is because it is much easier if you want to do anything with those items later (Such as mystery crates, and roulettes).
- Now you are ready to continue.
5) For this step, i'm assuming you have already chosen everything you need in terms of damage, and feed power, stat bonuses and name and description. You want to begin editing the XML by changing the sprite location. By default, the XML you make on Pfiffel will have the sprite selected to the item you chose on prod. Thus, you want to change the following things: `<File> (File Name) </File>` and `<Index> (Index) </Index>`. The file name is the name of your sprite sheet. For example, if your sprite sheet was named `EmbeddedAssets_customitemsEmbed_`, you would write `customitems`. The Index is the hex code of your item location. Go to [this](https://learn.sparkfun.com/tutorials/hexadecimal) site to learn. Basically, hex decimals include `0, 1, 2, 3, 4, 5, 6, 7, 8, 9, a, b, c, d, e, f`. For the index, you always want to start with 0x. Then, you want to want to work with the last sprite on the sheet, and work from there. Here are a few examples:
- If your pervious item was the projectiles Gift Parade (In lofiObj3), then then want to search Dat1.XML for your item. I found that it was `0x560, 0x561 and 0x562`. So, if I edit the sprite sheet and place my next sprite after the final gift parade (0x562), then I would put my next XML as `0x563`. This may seem complex, but by putting in a bit of time, you can understand this very simply. So, for the rest of the row, i'll put this: `0x564, 0x565, 0x566, 0x567, 0x568, 0x569, 0x56a, 0x56b, 0x56c, 0x56d, 0x56e, 0x56f`. Now, you won't be able to fit any more sprites in this row. So, you will go to the next row. The first few indexes for this row would be `0x570, 0x571, 0x572` and so on. Now, you are ready to select your index and sprite sheet.
6) Now, once you have successfully selected your sprite sheet and index, you will need to change only one other thing. It is the projectile to the item. If you are using one of the prod projectiles, which is already added to the server, then you can skip this step. Otherwise, you will have to change it. For the projectile, you can have `<ObjectId> (Name) </ObjectId)`. To find the name of the projectile, just search for the ID (Name). This will be the same as the ObjectID.

You have now created your own XML. Here is an example of what an XML should look like:

###### Doom Bow XML:

- ``<Object type="0xc02" id="Doom Bow">``   => This is the first line where you change the type and id.
- ``<Class>Equipment</Class>``   => This is where you change what type of item it is. For weapons, keep it as Equipment.
- ``<Item/>``   => This is just defining that it is an item.
- ``<Texture>``   => This is changing where it is texture in the client or a remote texture. Keep it as a texture, do not use remote texture.
- ``<File>lofiObj2</File>``   => This is where you define what file the texture is in.
- ``<Index>0x6a</Index>``   => This is where where tell the server where the texture actually is within the file.
- ``</Texture>``   => This is to tell the tell the server that it is the ending of the texture section.
- ``<SlotType>3</SlotType>``   => Slot type of the weapon. Varies as you change class. Read below for more info.
- ``<Description>No mortal can fire this dreaded bow without resting in betw</Description>``   => Create your fancy description of your item.
- ``<RateOfFire>0.33</RateOfFire>``   => This is how fast your projectile will shoot. Default is 1. As it gets lower, it shoots slower.
- ``<Sound>weapon/golden_bow</Sound>``   => Sound of the item.
- ``<Projectile>``   => Beginning of the projectile section.
- ``<ObjectId>Black Arrow</ObjectId>``   => Sprite of the projectile (name only).
- ``<Speed>140</Speed>``   => Speed of the projectile (how fast it travels).
- ``<MinDamage>400</MinDamage>``   => Minimum damage of the projectile.
- ``<MaxDamage>500</MaxDamage>``   => Maximum damage of the projectile.
- ``<LifetimeMS>503</LifetimeMS>``   => How long the projectile will stay alive (in milliseconds) before it disappears. This affects your range too.
- ``<MultiHit/>``   => This tells the server if the projectile passes through objects to hit other objects too.
- ``</Projectile>``   => The end of the projectile section.
- ``<BagType>6</BagType>``   => Bag type of the item. Look at the Hastebin below for more information.
- ``<FameBonus>6</FameBonus>``   => Fame bonus upon dying provided by your item.
- ``<NumProjectiles>1</NumProjectiles>``   => Number of projectiles for your item. (Number of shots it will shoot).
- ``<OldSound>arrowShoot</OldSound>``   => Sound of the item. Use just a default sound for a UT item if you can't think of anything.
- ``<feedPower>900</feedPower>``   => Feedpower of the item (For your pet).
- ``<Soulbound/>``   => This can be added/removed before the Display ID to make it tradable/un-tradable.
- ``<DisplayId>Doom Bow</DisplayId>``   => This is what name will be displayed on the item upon hovering over it.
- ``</Object>``   => End of the XML.

Refer to [this Hastebin]() to see the bag types, and more info (slot type and effects).

### Armor and Ring XML

Armor and Ring XMLS are very simple, as they have no special things to mention. You need to keep these things in mind:
- Armor and Ring XMLS are the easiest of the bunch.
- They follow the same principles as weapon XMLS, such as the type, the id, the texture and etc.
- Armor XMLS are class specific, so you will need to change the slot type.
- Ring XMLS are not class specific, so you can use the same format, just copy + paste and change the XML and change the boosts and basic details.
- You can use [this](https://mega.nz/#!GDBTxbaJ!r7i8u3CL-AEYsPDlMovZDmOwdsD9O8EllYEDob0nIGg) tool to create your Armor and Ring XMLS **only**. Use this if you are lazy.

Refer to [this Hastebin]() to see the bag types, and more info such as stat ids and slot types.


# Client Set Up:

FAQ:

**Q) Why use AS3?**

A) AS3 is a much more simpler client, that Deca Games currently uses for development. It is much easier to edit the code, and the posibilities are also greater. It also has no obfuscation, thus, it should be easier to write code. This means that you no longer need to use FFDEC/JPEXS and Rabdascm.

**Q) What programs do I need?**

A) [Flex SDK 4.9.1:](https://www.dropbox.com/s/8g4uiku6xw16mcl/flex_sdk_4.9.1.rar?dl=0)
   [Source Code for 27.X.X2:](https://github.com/kaos00723/RotMG_Client_27.7.X2)
   [IntelliJ Idea:](https://www.jetbrains.com/idea/download/)
   - [Windows:](https://www.jetbrains.com/idea/download/#section=windows)
   - [Mac:](https://www.jetbrains.com/idea/download/#section=mac)
   - [Linux:](https://www.jetbrains.com/idea/download/#section=linux)
You can use any version, however, I recommend you use premium version. Don't have the money to purchase it? Don't worry, you can always get in on trial, then reset your activate code, or just crack it. However, it is important you use this software, as it is optimised to make the server.

**Q) Do I need to do any server sided edits with this AS3 Client?**

A) No, I have already done all the necessary server-sided edits you need to do. It should be good to go.

**Q) Can this AS3 client be hacked using normal methods?**

A) Yes, in fact, it can. Therefore, you need to set up your own protection for it. This includes methods such as Obfuscation, Domain Lock, File Reduction, etc. However, I have provided the tutorial on how to protect your client from hacks in the following document.

**Q) Will this make my server exactly like Realm of the Mad God?**

A) No, this will not. This tutorial is simply to connect the AS3 client to FSoD. Functions like Pet Skins and Friends List won't work, however, I have added the latest prod assets to the client and server. You should download the latest en.txt and replace the one there is in server/app/Languages. Remote textures are off by default in the client.

### Procedure:

1)  **Navigate to com/company/assembleegameclient/parameters/Parameters.as**
    - Find: `public static const RSA_PUBLIC_KEY:String`  (Note: It will be in a long line, just replace the whole line).
    - Change to: `public static const RSA_PUBLIC_KEY:String = ((((("-----BEGIN PUBLIC KEY-----\n" + "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCbqweYUxzW0IiCwuBAzx6Htskr") + "hWW+B0iX4LMu2xqRh4gh52HUVu9nNiXso7utTKCv/HNK19v5xoWp3Cne23sicp2o") + "VGgKMFSowBFbtr+fhsq0yHv+JxixkL3WLnXcY3xREz7LOzVMoybUCmJzzhnzIsLP") + "iIPdpI1PxFDcnFbdRQIDAQAB\n") + "-----END PUBLIC KEY-----");`

2)  **Navigate to src/kabam/rotmg/messaging/impl/data/ObjectData.as**
    - Find: `this.objectType_ = _arg_1.readShort();`
    - Change to: `this.objectType_ = _arg_1.readUnsignedShort();`

3)  **Navigate to src/kabam/rotmg/messaging/impl/GameServerConnectionConcrete.as**
    - **Find:** `_local_2.buildVersion_ = ((Parameters.BUILD_VERSION + ".") + "0");`
    - **Change to:** `_local_2.buildVersion_ = ((Parameters.BUILD_VERSION + ".") + Parameters.MINOR_VERSION);`

4)  **Navigate to src/kabam/rotmg/application/impl/ProductionSetup.as**
    - Go to line 8. You want to find the Realm of the Mad God URL (realmofthemadgodhrd.appspot.com), and change it to your Domain/IP Adress, or simply to Localhost if you just want to run the server for yourself.
    - **Find:**  `private const SERVER:String =;`
    - **Change to:** `private const ENCRYPTED:String`
    - **Find:** `https`
    - **Change to:** `http`

Getting errors in the building of your client? Here are a few tips and common problems users get.
- Getting an error with Right_Click? Then make sure you are using Flex SDK 4.9.1, the one I have provided in the downloads section.

Now you are ready to use your client. Press file, then save all. Then, press project, and rebuild all.

###### Credits to: [WintersQ](https://www.mpgh.net/forum/showthread.php?t=1163271)

# Changes to this source compared to standard FSoD:

**Server sided fixes:**
- [x] Fixed up ranks and prefixes.
- [x] Fixed Pet Healing HP and Healing MP, along with the Pet Electric behaviours.
- [ ] Fixed all existing dungeons in the source, updated the maps, bosses and drops.
- [ ] Optimised the source to run efficiently (Fixed bugs).
- [ ] Added functioning pet skins to the server.

**Client sided fixes:**
- [x] Add fullscreen and map scale to the client.
- [ ] Add all the latest assets to the client.
- [ ] Fix issues with friends tab.
- [ ] Client protection and domain locking.
