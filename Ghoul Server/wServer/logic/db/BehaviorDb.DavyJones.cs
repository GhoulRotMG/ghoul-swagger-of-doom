#region

using wServer.logic.behaviors;
using wServer.logic.loot;
using wServer.logic.transitions;

#endregion

namespace wServer.logic
{
    partial class BehaviorDb
    {
        private _ DavyJones = () => Behav()
            .Init("Davy Jones",
                new State(
                    new RealmPortalDrop(),
                    new State("Idle",
                        new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                        new State("Floating",
                            new ChangeSize(100, 100),
                            new SetAltTexture(1),
                            new SetAltTexture(3),
                            new Wander(.2),
                            new StayCloseToSpawn(0.4, 10),
                            new Shoot(10, 5, 10, 0, coolDown: 2000),
                            new Shoot(10, 1, 10, 1, coolDown: 4000),
                            new EntityExistsTransition("Ghost Lanturn On", 99, "CheckOffLanterns")
                            ),
                        new State("CheckOffLanterns",
                            new SetAltTexture(2),
                            new ReturnToSpawn(once: true, speed: 1),
                            new Shoot(10, 5, 10, 0, coolDown: 2000),
                            new Shoot(10, 1, 10, 1, coolDown: 1000),
                            new EntityNotExistsTransition("Ghost Lanturn Off", 99, "Vunerable")
                            )
                        ),
                    new State("Vunerable",
                        new Order(99, "Ghost Lanturn On", "Stay Here"),
                        new TimedTransition(3000, "deactivate 1")
                        ),
                    new State("deactivate 1",
                        new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                        new State("deactivate 2",
                            new SetAltTexture(5),
                            new TimedTransition(1000, "deactivate 3")
                            ),
                        new State("deactivate 3",
                            new Order(99, "Ghost Lanturn On", "Shoot"),
                            new SetAltTexture(6),
                            new TimedTransition(1000, "Idle")
                            )
                        )
                    ),
                new MostDamagers(4,
                    new ItemLoot("Potion of Wisdom", 1),
                    new ItemLoot("Greater Potion of Wisdom", 0.1)
                    ),
                new MostDamagers(5,
                    new ItemLoot("Potion of Attack", 0.1)
                    ),
                new Threshold(0.001,
                new TierLoot(3, ItemType.Ring, 0.2),
                new TierLoot(7, ItemType.Armor, 0.2),
                new TierLoot(8, ItemType.Weapon, 0.2),
                new TierLoot(3, ItemType.Ability, 0.12),
                new TierLoot(4, ItemType.Ability, 0.1),
                new TierLoot(8, ItemType.Armor, 0.12),
                new TierLoot(4, ItemType.Ring, 0.07),
                new TierLoot(9, ItemType.Armor, 0.10),
                new TierLoot(5, ItemType.Ability, 0.04),
                new TierLoot(8, ItemType.Weapon, 0.12),
                new TierLoot(9, ItemType.Weapon, 0.10),
                new TierLoot(10, ItemType.Armor, 0.02),
                new TierLoot(10, ItemType.Weapon, 0.06),
                new TierLoot(11, ItemType.Armor, 0.01),
                new TierLoot(11, ItemType.Weapon, 0.01),
                new TierLoot(5, ItemType.Ring, 0.01),
                new ItemLoot("Spirit Dagger", 0.01),
                new ItemLoot("Wine Cellar Incantation", 0.01),
                new ItemLoot("Spectral Cloth Armor", 0.01),
                new ItemLoot("Ghostly Prism", 0.01),
                new ItemLoot("Captain's Ring", 0.01),
                new ItemLoot("Ghostly Prism", 0.01),
                new ItemLoot("Captain's Ring", 0.01),
                new ItemLoot("Golden Chalice", 0.03),
                new ItemLoot("Ruby Gemstone", 0.03),
                new ItemLoot("Pearl Necklace", 0.03)
                )
            )
        .Init("Purple Key",
            new State(
                new ConditionalEffect(ConditionEffectIndex.Invincible, true),
             new State("Idle",
                new PlayerWithinTransition(1, "Despawn")
                ),
            new State("Despawn",
                new Suicide()
                )
            )
            )
        .Init("Green Key",
            new State(
                new ConditionalEffect(ConditionEffectIndex.Invincible, true),
             new State("Idle",
                new PlayerWithinTransition(1, "Despawn")
                ),
            new State("Despawn",
                new Suicide()
                )
            )
            )
        .Init("Red Key",
            new State(
                new ConditionalEffect(ConditionEffectIndex.Invincible, true),
             new State("Idle",
                new PlayerWithinTransition(1, "Despawn")
                ),
            new State("Despawn",
                new Suicide()
                )
            )
            )
        .Init("Yellow Key",
            new State(
                new ConditionalEffect(ConditionEffectIndex.Invincible, true),
             new State("Idle",
                new PlayerWithinTransition(1, "Despawn")
                ),
            new State("Despawn",
                new Suicide()
                )
            )
            )
        .Init("GhostShip PurpleDoor Rt",
            new State(
                new State("Idle",
                    new EntityNotExistsTransition("Purple Key", 999, "PreOpen")
                    ),
                new State("PreOpen",
                    new PlayerWithinTransition(2, "Open")
                    ),
                new State("Open",
                    new Decay(0)
                    )
                )
            )
        .Init("GhostShip PurpleDoor Lf",
            new State(
                new State("Idle",
                    new EntityNotExistsTransition("Purple Key", 999, "PreOpen")
                    ),
                new State("PreOpen",
                    new PlayerWithinTransition(2, "Open")
                    ),
                new State("Open",
                    new Decay(0)
                    )
                )
            )
        .Init("GhostShip GreenDoor Rt",
            new State(
                new State("Idle",
                    new EntityNotExistsTransition("Green Key", 999, "PreOpen")
                    ),
                new State("PreOpen",
                    new PlayerWithinTransition(2, "Open")
                    ),
                new State("Open",
                    new Decay(0)
                    )
                )
            )
        .Init("GhostShip GreenDoor Lf",
            new State(
                new State("Idle",
                    new EntityNotExistsTransition("Green Key", 999, "PreOpen")
                    ),
                new State("PreOpen",
                    new PlayerWithinTransition(2, "Open")
                    ),
                new State("Open",
                    new Decay(0)
                    )
                )
            )
        .Init("GhostShip RedDoor Rt",
            new State(
                new State("Idle",
                    new EntityNotExistsTransition("Red Key", 999, "PreOpen")
                    ),
                new State("PreOpen",
                    new PlayerWithinTransition(2, "Open")
                    ),
                new State("Open",
                    new Decay(0)
                    )
                )
            )
        .Init("GhostShip RedDoor Lf",
            new State(
                new State("Idle",
                    new EntityNotExistsTransition("Red Key", 999, "PreOpen")
                    ),
                new State("PreOpen",
                    new PlayerWithinTransition(2, "Open")
                    ),
                new State("Open",
                    new Decay(0)
                    )
                )
            )
        .Init("GhostShip YellowDoor Rt",
            new State(
                new State("Idle",
                    new EntityNotExistsTransition("Yellow Key", 999, "PreOpen")
                    ),
                new State("PreOpen",
                    new PlayerWithinTransition(2, "Open")
                    ),
                new State("Open",
                    new Decay(0)
                    )
                )
            )
        .Init("GhostShip YellowDoor Lf",
            new State(
                new State("Idle",
                    new EntityNotExistsTransition("Yellow Key", 999, "PreOpen")
                    ),
                new State("PreOpen",
                    new PlayerWithinTransition(2, "Open")
                    ),
                new State("Open",
                    new Decay(0)
                    )
                )
            )
        .Init("GhostShip Rat",
            new State(
                new Follow(0.1, range: 1),
                new Wander(0.3),
                new Shoot(3, 3, 1, 0, coolDown: 50, predictive: 0.3)
                )
            )
            .Init("Lil' Ghost Pirate",
                new State(
                    new NoPlayerWithinTransition(3, "Idle"),
                        new State("Shoot",
                            new Shoot(6, 1, 1, 0, coolDown: 50, predictive: 0.2)
                            ),
                        new State("Idle",
                            new ConditionalEffect(ConditionEffectIndex.Invincible),
                            new PlayerWithinTransition(3, "Shoot")
                            )
                        )
                    )
        .Init("Ghost of Roger",
            new State(
                new EntityNotExistsTransition("Lost Soul", 5, "ReSpawnSpirits"),
                new Wander(0.2),
                new State("Shoot",
                    new Shoot(8, 1, 1, 0, coolDown: 60),
                    new Shoot(8, 1, 1, 0, coolDown: 60, coolDownOffset: 40),
                    new Shoot(8, 1, 1, 0, coolDown: 60, coolDownOffset: 60),
                    new TimedTransition(60, "Pause")
                    ),
                new State("Pause",
                    new TimedTransition(500, "Shoot")
                    ),
                new State("ReSpawnSpirits",
                    new Spawn("Lost Soul", 5),
                    new TimedTransition(100, "Shoot")
                    )
                )
            )
            .Init("Lost Soul",
            new State(
                new Wander(0.1),
                new Shoot(2, 5, projectileIndex: 0, coolDown: 500)
            )
        )
            .Init("Ghost Lanturn Off",
                new State(
                    new TransformOnDeath("Ghost Lanturn On")
                    )
            )
            .Init("Ghost Lanturn On",
                new State(
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                    new State("idle",
                        new TimedTransition(8000, "gone")
                        ),
                    new State("Stay Here"),
                    new State("Shoot",
                        new Shoot(10, 6, coolDown: 9000001, coolDownOffset: 100),
                        new TimedTransition(250, "gone")
                        ),
                    new State("gone",
                        new Transform("Ghost Lanturn Off")
                        )
                    )
            );
    }
}
