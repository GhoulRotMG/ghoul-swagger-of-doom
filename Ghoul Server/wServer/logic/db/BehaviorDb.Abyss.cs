using wServer.logic.behaviors;
using wServer.logic.transitions;
using wServer.logic.loot;

namespace wServer.logic
{
    partial class BehaviorDb
    {
        private _ Abyss = () => Behav()
            .Init("Archdemon Malphas",
                new State(
                    new OnDeathBehavior(new ApplySetpiece("AbyssDeath")),
                    new State("default",
                        new PlayerWithinTransition(8, "WarmUp")
                        ),
                        new State("WarmUp",
                        new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                        new Taunt(1.0, "Welcome to the Abyss of the Demons, but unfortunately you won't make it out of here ALIVE!!!", "Help me kind soldier, I am stuck in this demon body. Kill me to free my soul...", "Hey Infernape, is that you? Oh no, it is an imposter... Prepare to face my WRATH!!!"),
                        new TimedTransition(2000, "basic")
                        ),
                    new State("basic",
                        new Prioritize(
                            new Follow(0.3),
                            new Wander(0.2)
                            ),
                        new Reproduce("Malphas Missile", densityMax: 4, spawnRadius: 5, coolDown: 1000),
                        new Shoot(15, predictive: 1, coolDown: 800),
                        new TimedTransition(8000, "shrink")
                        ),
                    new State("shrink",
                        new Wander(0.4),
                        new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                        new ChangeSize(-15, 25),
                        new TimedTransition(1000, "smallAttack")
                        ),
                    new State("smallAttack",
                        new Prioritize(
                            new Follow(1, acquireRange: 15, range: 8),
                            new Wander(1)
                            ),
                        new Shoot(10, predictive: 1, coolDown: 750),
                        new Shoot(10, 6, projectileIndex: 1, predictive: 1, coolDown: 1000),
                        new Taunt(1.0, "How dare you disturb me! I still have {HP} health to destroy you!", "Leave now, hero. I am giving you a chance to change your fortune.", "Hmm, stubborn aren't you, well I guess I just have to show you my wrath!"),
                        new TimedTransition(10000, "grow")
                        ),
                    new State("grow",
                        new Wander(0.1),
                        new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                        new ChangeSize(35, 200),
                        new TimedTransition(1050, "bigAttack")
                        ),
                    new State("bigAttack",
                        new Prioritize(
                            new Follow(0.2),
                            new Wander(0.1)
                            ),
                        new Shoot(10, projectileIndex: 2, predictive: 1, coolDown: 2000),
                        new Shoot(10, projectileIndex: 2, predictive: 1, coolDownOffset: 300, coolDown: 2000),
                        new Shoot(10, 3, projectileIndex: 3, predictive: 1, coolDownOffset: 100, coolDown: 2000),
                        new Shoot(10, 3, projectileIndex: 3, predictive: 1, coolDownOffset: 400, coolDown: 2000),
                        new TimedTransition(10000, "normalize")
                        ),
                    new State("normalize",
                        new Wander(0.3),
                        new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                        new ChangeSize(-20, 100),
                        new TimedTransition(1000, "basic")
                        )
                    ),
                new MostDamagers(5,
                    new ItemLoot("Potion of Vitality", 1.0)
                ),
                new MostDamagers(1,
                    new ItemLoot("Potion of Defense", 0.4)
                ),
                new Threshold(0.0025,
                    new TierLoot(9, ItemType.Weapon, 0.1),
                    new TierLoot(4, ItemType.Ability, 0.1),
                    new TierLoot(9, ItemType.Armor, 0.1),
                    new TierLoot(3, ItemType.Ring, 0.05),
                    new TierLoot(10, ItemType.Armor, 0.05),
                    new TierLoot(10, ItemType.Weapon, 0.05),
                    new TierLoot(4, ItemType.Ring, 0.025),
                    new ItemLoot("Demon Blade", 0.001),
                    new ItemLoot("Abyss of Demons Key", 0.1),
                    new ItemLoot("Sword of Illumination", 0.001)
                )
            )
            .Init("Abyss Idol",
            new State(
            ),
            new MostDamagers(5,
            new ItemLoot("Potion of Vitality", 1.0),
            new ItemLoot("Greater Potion of Vitality", 0.9)
            ),
            new MostDamagers(5,
            new ItemLoot("Potion of Defense", 1.0),
            new ItemLoot("Greater Potion of Defense", 0.9)
            ),
            new Threshold(0.0025,
            new TierLoot(10, ItemType.Weapon, 0.1),
            new EggLoot(EggRarity.Common, 0.1),
            new EggLoot(EggRarity.Uncommon, 0.05),
            new EggLoot(EggRarity.Legendary, 0.001),
            new TierLoot(5, ItemType.Ability, 0.1),
            new TierLoot(4, ItemType.Ability, 0.1),
            new TierLoot(10, ItemType.Armor, 0.1),
            new TierLoot(4, ItemType.Ring, 0.05),
            new TierLoot(11, ItemType.Armor, 0.05),
            new TierLoot(11, ItemType.Weapon, 0.05),
            new TierLoot(5, ItemType.Ring, 0.025),
            new ItemLoot("Sword of Illumination", 0.001),
            new ItemLoot("Abyss of Demons Key", 0.1),
            new ItemLoot("Demon Blade", 0.001)
            )
        )
            .Init("Malphas Missile",
                new State(
                    new State(
                        new Prioritize(
                            new Follow(0.8, range: 8),
                            new Wander(0.8)
                        ),
                        new HpLessTransition(0.5, "die"),
                        new TimedTransition(2000, "die")
                    ),
                    new State("die",
                        new Flash(0xFFFFFF, 0.2, 5),
                        new TimedTransition(2000, "explode")
                        ),
                    new State("explode",
                        new Shoot(10, 8),
                        new Decay(100)
                        )
                    )
            )
            .Init("Imp of the Abyss",
                new State(
                    new Wander(0.875),
                    new Shoot(8, 5, 10, coolDown: 1000)
                    ),
                new ItemLoot("Health Potion", 0.1),
                new ItemLoot("Magic Potion", 0.1),
                new Threshold(0.5,
                    new ItemLoot("Cloak of the Red Agent", 0.01),
                    new ItemLoot("Felwasp Toxin", 0.01)
                    )
            )
            .Init("Demon of the Abyss",
                new State(
                    new Prioritize(
                        new Follow(1, 8, 5),
                        new Wander(0.25)
                        ),
                    new Shoot(8, 3, shootAngle: 10, coolDown: 1000)
                    ),
                new ItemLoot("Fire Bow", 0.05),
                new Threshold(0.5,
                    new ItemLoot("Mithril Armor", 0.01)
                    )
            )
            .Init("Demon Warrior of the Abyss",
                new State(
                    new Prioritize(
                        new Follow(1, 8, 5),
                        new Wander(0.25)
                        ),
                    new Shoot(8, 3, shootAngle: 10, coolDown: 1000)
                    ),
                new ItemLoot("Fire Sword", 0.025),
                new ItemLoot("Steel Shield", 0.025)
            )
            .Init("Demon Mage of the Abyss",
                new State(
                    new Prioritize(
                        new Follow(1, 8, 5),
                        new Wander(0.25)
                        ),
                    new Shoot(8, 3, shootAngle: 10, coolDown: 1000)
                    ),
                new ItemLoot("Fire Nova Spell", 0.02),
                new Threshold(0.1,
                    new ItemLoot("Wand of Dark Magic", 0.01),
                    new ItemLoot("Avenger Staff", 0.01),
                    new ItemLoot("Robe of the Invoker", 0.01),
                    new ItemLoot("Essence Tap Skull", 0.01),
                    new ItemLoot("Demonhunter Trap", 0.01)
                    )
            )
            .Init("Brute of the Abyss",
                new State(
                    new Prioritize(
                        new Follow(1.5, 8, 1),
                        new Wander(0.25)
                        ),
                    new Shoot(8, 3, shootAngle: 10, coolDown: 500)
                    ),
                new ItemLoot("Health Potion", 0.1),
                new Threshold(0.1,
                    new ItemLoot("Obsidian Dagger", 0.02),
                    new ItemLoot("Steel Helm", 0.02)
                    )
            )
            .Init("Brute Warrior of the Abyss",
                new State(
                    new Prioritize(
                        new Follow(1, 8, 1),
                        new Wander(0.25)
                        ),
                    new Shoot(8, 3, shootAngle: 10, coolDown: 500)
                    ),
                new ItemLoot("Spirit Salve Tome", 0.02),
                new Threshold(0.5,
                    new ItemLoot("Glass Sword", 0.01),
                    new ItemLoot("Ring of Greater Dexterity", 0.01),
                    new ItemLoot("Magesteel Quiver", 0.01)
                    )
            )
            ;
    }
}
